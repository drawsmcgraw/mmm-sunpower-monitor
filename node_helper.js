var NodeHelper = require("node_helper")
const axios = require("axios");

module.exports = NodeHelper.create({
  start: function() {
  },
  socketNotificationReceived: function (notification, payload) {
    switch (notification) {
      case "sunpower-fetch-data":
        if (!this.requestInProgress) {
          //console.log("Attempting connection to " + payload.sunpowerHostname)
          this.requestInProgress = true;
          // fetch data from the Sunpower appliance
          axios({
            method: 'get',
            url: payload.sunpowerHostname + '/cgi-bin/dl_cgi?Command=DeviceList',
            retry: {
              retries: 5,
              retryDelay: (retryCount) => {
                return retryCount * 1000; // Incremental backoff strategy
              },
              retryCondition: (error, response) => {
                // Retry if the error is a network error or a 5xx server error
                return (
                  axios.isRetryableError(error) ||
                  (error.response && error.response.status >= 500) ||
                  !response.data
                );
              }
            }
          })
            .then(response => {
              //console.log(response.data);
              //console.log("Calculating total power");
              var totalkw = 0;
              var totalkwh = 0;

              // response is an array of panel information. 
              // sum the 'p_3phsum_kw' field of all panels to
              // get total power generation of the system.
              for (var i = 0; i < response.data.devices.length; i++) {
                var device = response.data.devices[i];
                if (device.TYPE == "SOLARBRIDGE") {
                  if (device.p_3phsum_kw) {
                    totalkw += parseFloat(device.p_3phsum_kw);
                  };
                  if (device.net_ltea_3phsum_kwh) {
                    totalkwh += parseFloat(device.net_ltea_3phsum_kwh);
                  };
                }
                
              }
              returnData = {
                totalkw: totalkw.toFixed(2),
                totalkwh: totalkwh.toFixed(2)
              }

              //console.log("Returning solar power data");
              this.sendSocketNotification("sunpower-data-fetched", returnData);
              this.requestInProgress = false;
            })
            .catch(error => {
              console.error("MMM-sunpower: " + error.cause.code);
              this.requestInProgress = false;
            })
        };
        break
    }
  },
})
